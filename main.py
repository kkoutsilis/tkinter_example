import tkinter
import tkinter.messagebox
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# Connect to the database
engine = create_engine("sqlite:///database.db")
Base = declarative_base()

# Define the user data model
class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)

# Create the table in the database
Base.metadata.create_all(engine)

# Create a session to execute SQL commands
Session = sessionmaker(bind=engine)
session = Session()

# Create the GUI
root = tkinter.Tk()
root.title("Save User Data")

# Create the form
tkinter.Label(root, text="Name").grid(row=0)
name_entry = tkinter.Entry(root)
name_entry.grid(row=0, column=1)

tkinter.Label(root, text="Email").grid(row=1)
email_entry = tkinter.Entry(root)
email_entry.grid(row=1, column=1)

# Define the save function
def save():
    # Get the user data from the form
    name = name_entry.get()
    email = email_entry.get()

    # Validate the input
    if name == "":
        tkinter.messagebox.showerror("Error", "Please enter a name")
        return
    if email == "":
        tkinter.messagebox.showerror("Error", "Please enter an email address")
        return

    # Insert the data into the database
    user = User(name=name, email=email)
    session.add(user)
    session.commit()

    # Display a confirmation message
    tkinter.messagebox.showinfo("Success", "User data saved successfully")

# Create the save button
save_button = tkinter.Button(root, text="Save", command=save)
save_button.grid(row=2, column=1)

root.mainloop()
