# Simple python application to see how tkinter works.

## How to run

- Create virtual environment.
  ```bash
  python3 -m venv env
  source env/bin/activate
  ```
- Install requirements.
  ```bash
  pip install -r requirements.txt
  ```
- Run main.py.
  ```bash
  python3 main.py
  ```
